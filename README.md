# PoC_Prometheus_Postgres

This repository allows to test exporters for Prometheus integrated with Postgres. 

## Requirements

- docker
- docker-compose

## HOWTO

```
docker-compose up
```

## Access

**postgres:**
user: `postgres`
password: `postgres`

```
psql -h localhost -p 5434 postgres postgres
```

**postgres-exporter:** 

```
http://localhost:9187/metrics
```

**prometheus:** 

```
http://localhost:9090
```

**grafana:** 
user: `admin`
password: `admin`

```
http://localhost:3000
```